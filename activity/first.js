// console.log('hello');

let fullName = prompt("Enter your Full Name");
console.log("Full Name: " +fullName);

let myAge = prompt('Enter your Age');
console.log("My Age is: " + myAge);

let address = prompt('Enter your Address');
console.log("My Address is in: " + address);

alert('Thank you for your inputs');


function favoriteBand(){
	const countryBands = ['Brooks & Dunn', 'Alabama', 'The Chicks', 'The Highwaymen', 'Rascal Flatts']
	
	for (let x = 0; x < countryBands.length; x++){
		console.log(x + 1 + ". " + countryBands[x]);
	};
};

favoriteBand();

function favoriteMovies(){
	const myMoviesWithRating = [
	{ movie: "The Good Doctor", rating: "67%"},
	{ movie: "Amsterdam", rating: "73%"},
	{ movie: "The Greatest Showman", rating: "56%"},
	{ movie: "Angels Has Fallen", rating: "72%"},
	{ movie: "Ready Player One", rating: "72%"}
	];

	for ( let x = 0; x < myMoviesWithRating.length; x++) {
		console.log( x + 1 + ". " + myMoviesWithRating[x].movie);
		console.log( "Rotten Tomatoes Rating: " + myMoviesWithRating[x].rating);
	};
};

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);