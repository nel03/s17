// console.log('hellow');

/*
	Functions
		>are lines/block of codes that tell our 
		device/ application to perform certain tasks 
		when called/ invoked

		> are mostly created to create complicated tasks
		to run several lines of code in succession

		> they are also used to prevent repeating lines/
		blocks of code that perform the same task/function

	syntax:
		function functionName(){
			code block(statement)
		}

		>function keyword
			>used to define a javascript functions

		>fuctionName
			> fuction name. function are named to be able to
			use later in the code

		>function block {}
			>statements which comprise the body of the function
			this is where the code to be executed
		
*/
function printName(){
	console.log('My name is Ronel. I teach Bookkeeping')
};

/*
	function invocation
		>it is common to use the term "call a function"
		instead of "invoke a function"

*/
printName();


declaredFunction();
// result: error, because it is not yet defined

function declaredFunction(){
	console.log('this is a defined function');
};

/*
	function declaration vs expression
		> a function can be created through function
		declaration by using the function keyword and
		add a function name

		> Declared Function are not executed immediately
		they are "save for later use" and will be executed
		later, when they are invoked (called upon)
*/

function declaredFunction2(){
	console.log('this is a function declaration');
};

declaredFunction2();

/*
	function expression
		>a function can also be stored in a variable
		this is called afunction expression

		>function expression is an anonymous function
		assigned to the variable function

		>anonymous function
			>function withou a name
*/

let variableFunction = function(){
	console.log('i am from variable function');
};

variableFunction();


/*
	we can also create a function expression of a named
	function. however, to invoke the function expression
	we invoke it by its variable name not its function
	name

	function expressions are always invoke (called) using
	the variable name
*/
let funcExpression = function funcName(){
	console.log('Hello from the other side.');
};

funcExpression();

/*
	you can reassign declared function and function
	expression to new anonymous fuction
*/

declaredFunction = function (){
	console.log('updated declared function');
};

declaredFunction();

funcExpression = function (){
	console.log('updated function expression');
};

funcExpression();

const constantFunction = function (){
	console.log('initialized with const');
};

constantFunction();

/*
	function scoping
		>scope
			> is the aceessibility (visibility) of
			variables within our program
			>javascrip variables has 3 types of scope
			1. local/block scope
			2. global scope
			3. function scope


*/

{
	let localVar = "Tom Cruise";
}

let globalVar = "The world player";

// console.log(localVar); a local variable will be visible only within a function, were it is defined
console.log(globalVar); // has a globel scope which means it can be defined anywhere in your JS code

/*
	function scope

	JS has a function scope: each function creates a new scope
	variables defined inside a function are not accessible
	(visible) from outside the function
	variable declared with var, let and const are
	quite similar when declared inside a fucntion
*/	

function showNames(){
	// fucntion scope
	var functionVar = "jungkook";
	const functionConst = "BTS";
	let functionLet = "kookie";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

/*
	Nested Function
		> you can create another function inside a
		function. this is called a nested function.
		this nested function, being inside a new
		function will have access variable, name as
		they are within the same scope/code block
*/

function myNewFunction(){
	let name = "Yor";

	function nestedFunction (){
		let nestedName = "brando";
		console.log(nestedName);
	};
	nestedFunction();
};
myNewFunction();

/*
	Function and Global Scoped variables

	Global Scope variable
*/

let globalName = 'Nel'

function myNewFunction2(){
	let nameInside = "Ronel"
	console.log(globalName);
};

myNewFunction2();

/*
	alert()
	syntax: alert("message");
*/

/*
prompt()
	syntax:
		prompt("dialog")
*/
let samplePrompt = prompt("enter your name: ");
console.log("hello "+ samplePrompt);

let sampleNullPrompt = prompt("Don't input anything");
console.log(sampleNullPrompt);

/*
	if prompt() is cancelled, the result will be: null
	if there is no input in the prompt the result
	will be be empty string
*/
function printWelcomMessage(){
	let firstName = prompt("Enter your First Name: ");
	let lastName = prompt("Enter your last name");
	console.log("Hello " + firstName + lastName + "!");
	console.log("Welcom to Gamer's Guild");

};
printWelcomMessage();

/*
	Function naming converntion
		function should be definitive of its task
		usually contains a verb
*/

function getCourses(){
	let courses = ['Programming 100', 'Science 101' , 'Grammar 102', 'Mathematics 103']
	console.log(courses);
};	

getCourses();

// avoid generic names to avoid confusion like get
function get(){
	let name = "jimmin";
	console.log(name);
};
get();

/*
	name function in smallcaps. follow camelcase when
	naming function

*/

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");

}

displayCarInfo();